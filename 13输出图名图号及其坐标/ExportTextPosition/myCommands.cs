﻿// (C) Copyright 2020 by  
//
using Autodesk.AutoCAD.Runtime;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;




// This line is not mandatory, but improves loading performances
[assembly: CommandClass(typeof(ExportTextPosition.MyCommands))]

namespace ExportTextPosition
{

    // This class is instantiated by AutoCAD for each document when
    // a command is called by the user the first time in the context
    // of a given document. In other words, non static data in this class
    // is implicitly per-document!
    public class MyCommands
    {
        // The CommandMethod attribute can be applied to any public  member 
        // function of any public class.
        // The function should take no arguments and return nothing.
        // If the method is an intance member then the enclosing class is 
        // intantiated for each document. If the member is a static member then
        // the enclosing class is NOT intantiated.
        //
        // NOTE: CommandMethod has overloads where you can provide helpid and
        // context menu.

        // Modal Command with localized name
        [CommandMethod("ETP", CommandFlags.Modal)]

        
        public void ExportTextPosition() // This method can have any name
        {
            Form1 myfrom = new Form1();
            //  Autodesk.AutoCAD.ApplicationServices.Application.ShowModalDialog(myfrom); //在CAD里头显示界面  模态显示

            Autodesk.AutoCAD.ApplicationServices.Application.ShowModelessDialog(myfrom); //在CAD里头显示界面  非模态显示
                                                                                         // myfrom.sh
        }
    }
}
